package app.repositories;

import app.db.RelationsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RelationsRepository extends JpaRepository<RelationsEntity, Long> {
}
