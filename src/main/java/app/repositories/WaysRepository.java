package app.repositories;

import app.db.WaysEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WaysRepository extends JpaRepository<WaysEntity, Long> {
}
