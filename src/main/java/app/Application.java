package app;

import app.db.NodesEntity;
import app.db.RelationsEntity;
import app.db.WaysEntity;
import app.osm.Node;
import app.osm.Relation;
import app.osm.Way;
import app.repositories.NodesRepository;
import app.repositories.RelationsRepository;
import app.repositories.WaysRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.*;
import javax.xml.stream.events.XMLEvent;
import java.io.*;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

@SpringBootApplication
public class Application {
    private static XMLInputFactory inputFactory = XMLInputFactory.newInstance();
    private static SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

    private EntityManagerFactory emf;

    public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@PersistenceUnit
	public void setEntityManagerFactory(EntityManagerFactory emf) {
        this.emf = emf;
    }

	//@Bean @Order(0)
	public CommandLineRunner populateDatabase() {
        return args -> {
            String fileName = "data\\RU-NVS.osm";

            XMLEventReader reader = inputFactory.createFilteredReader(
                    inputFactory.createXMLEventReader(new FileReader(fileName)),
                    new EventFilter() {
                        @Override
                        public boolean accept(XMLEvent event) {
                            if (!event.isStartElement() && !event.isEndElement()) return false;
                            QName elementName = (event.isStartElement() ?
                                    event.asStartElement().getName() : event.asEndElement().getName()
                            );
                            return elementIsNeeded(elementName.getLocalPart());
                        }

                        private boolean elementIsNeeded(String name) {
                            return !("osm".equals(name) || "bounds".equals(name));
                        }
                    }
            );

            Unmarshaller u = JAXBContext.newInstance("app.osm").createUnmarshaller();

            EntityManager em = emf.createEntityManager();
            em.getTransaction().begin();

            int counter = 0;
            while (reader.hasNext()) {
                Object o = u.unmarshal(reader);
                Object entity = null;

                if (o instanceof Node) {
                    entity = new NodesEntity((Node) o);
                }
                else if (o instanceof Way) {
                    entity = new WaysEntity((Way) o);
                }
                else if (o instanceof Relation) {
                    entity = new RelationsEntity((Relation) o);
                }

                if (null != entity) {
                    em.persist(entity);
                    if (++counter % 10_000 == 0) {
                        System.out.println(counter + " entities persisted");
                        em.flush();
                        em.clear();
                        break;
                    }
                }
            }

            em.getTransaction().commit();
            em.close();
            reader.close();
        };
    }

    @Bean @Order(1)
    public CommandLineRunner loadRecentChangesets(NodesRepository nodes, WaysRepository ways, RelationsRepository relations) {
        return args -> {
            Scanner timeReader = new Scanner(new File("data\\time"));
            long previousRequestTime = timeReader.nextLong();
            timeReader.close();
            Writer timeWriter = new FileWriter("data\\time", false);
            timeWriter.write(Long.toString(System.currentTimeMillis()));
            timeWriter.close();

            String strTime = timeFormat.format(new Date(previousRequestTime));
            System.out.println("Loading new changesets since " + strTime);
            URL recentChangesets = new URL(
                    "http://api.openstreetmap.org/api/0.6/changesets?bbox=52.6734314,52.673501,80.872674,80.8727167&time=" + strTime
            );
            XMLStreamReader changesetsReader = inputFactory.createFilteredReader(
                    inputFactory.createXMLStreamReader(new InputStreamReader(recentChangesets.openStream())),
                    reader1 -> reader1.getEventType() == XMLEvent.START_ELEMENT && "changeset".equals(reader1.getLocalName())
            );

            List<String> changesetIDs = new ArrayList<>();
            while (changesetsReader.hasNext()) {
                changesetIDs.add(changesetsReader.getAttributeValue(null, "id"));
                changesetsReader.next();
            }

            Unmarshaller u = JAXBContext.newInstance("app.osm").createUnmarshaller();

            for (String changesetID : changesetIDs) {
                URL changeset = new URL("http://api.openstreetmap.org/api/0.6/changeset/" + changesetID + "/download");
                XMLEventReader reader = inputFactory.createFilteredReader(
                        inputFactory.createXMLEventReader(new InputStreamReader(changeset.openStream())),
                        event -> {
                            if (!event.isStartElement() && !event.isEndElement()) return false;
                            QName elementName = (event.isStartElement() ?
                                    event.asStartElement().getName() : event.asEndElement().getName()
                            );
                            return !"osmChange".equals(elementName.getLocalPart());
                        }
                );

                while (reader.hasNext()) {
                    String action = reader.nextEvent().asStartElement().getName().getLocalPart();
                    Object o = u.unmarshal(reader);
                    reader.nextEvent();

                    switch (action) {
                        case "create":
                        case "modify":
                            if (o instanceof Node) nodes.save(new NodesEntity((Node) o));
                            else if (o instanceof Way) ways.save(new WaysEntity((Way) o));
                            else if (o instanceof Relation) relations.save(new RelationsEntity((Relation) o));
                            break;
                        case "delete":
                            if (o instanceof Node) nodes.delete(new NodesEntity((Node) o));
                            else if (o instanceof Way) ways.delete(new WaysEntity((Way) o));
                            else if (o instanceof Relation) relations.delete(new RelationsEntity((Relation) o));
                            break;
                    }
                }

                nodes.flush();
                ways.flush();
                relations.flush();
            }
        };
    }
}
