package app.db;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.val;
import app.osm.Way;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Entity @Table(name = "ways")
@Getter @Setter @EqualsAndHashCode
public class WaysEntity {
    @Id private long id;
    @Basic private String username;
    @Basic private Long uid;
    @Basic private Boolean visible;
    @Basic private Long version;
    @Basic private Long changeset;
    @Basic private Timestamp timestamp;

    @Convert(converter = LongListToDBArrayConverter.class)
    @Column(columnDefinition = "bigint array")
    private ArrayList<Long> nodes = new ArrayList<>();

    @Convert(converter = app.db.MapToHStoreConverter.class)
    @Column(columnDefinition = "hstore")
    private Map<String, String> tags = new HashMap<>();

    public WaysEntity() {}

    public WaysEntity(Way way) {
        id = way.getId().longValueExact();
        username = way.getUser();
        uid = way.getUid().longValueExact();
        visible = way.isVisible();
        version = way.getVersion().longValueExact();
        changeset = way.getChangeset().longValueExact();
        timestamp = new Timestamp(way.getTimestamp().toGregorianCalendar().getTimeInMillis());

        for (val nd : way.getNd()) {
            nodes.add(nd.getRef().longValueExact());
        }

        for (val tag : way.getTag()) {
            tags.put(tag.getK(), tag.getV());
        }
    }
}
