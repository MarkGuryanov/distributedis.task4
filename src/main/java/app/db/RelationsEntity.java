package app.db;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.val;
import app.osm.Relation;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Entity @Table(name = "relations")
@Getter @Setter @EqualsAndHashCode
public class RelationsEntity {
    @Id private long id;
    @Basic private String username;
    @Basic private Long uid;
    @Basic private Boolean visible;
    @Basic private Long version;
    @Basic private Long changeset;
    @Basic private Timestamp timestamp;

    @Convert(converter = app.db.MemberListToDBArrayConverter.class)
    @Column(columnDefinition = "Member array")
    private ArrayList<Member> members = new ArrayList<>();

    @Convert(converter = app.db.MapToHStoreConverter.class)
    @Column(columnDefinition = "hstore")
    private Map<String, String> tags = new HashMap<>();

    public RelationsEntity() {}

    public RelationsEntity(Relation rel) {
        id = rel.getId().longValueExact();
        username = rel.getUser();
        uid = rel.getUid().longValueExact();
        visible = rel.isVisible();
        version = rel.getVersion().longValueExact();
        changeset = rel.getChangeset().longValueExact();
        timestamp = new Timestamp(rel.getTimestamp().toGregorianCalendar().getTimeInMillis());

        for (val member : rel.getMember()) {
            members.add(new Member(member));
        }

        for (val tag : rel.getTag()) {
            tags.put(tag.getK(), tag.getV());
        }
    }
}
