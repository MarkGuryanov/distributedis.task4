package app.db;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.ArrayList;
import java.util.List;

@Converter(autoApply = true)
public class LongListToDBArrayConverter implements AttributeConverter<List<Long>, String> {
    @Override
    public String convertToDatabaseColumn(List<Long> attribute) {
        if (null == attribute || 0 == attribute.size()) {
            return "{}";
        }

        StringBuilder sb = new StringBuilder("{");
        for (Long l : attribute) {
            sb.append(l.toString());
            sb.append(",");
        }
        sb.setLength(sb.length() - 1);
        sb.append("}");
        return sb.toString();
    }

    @Override
    public List<Long> convertToEntityAttribute(String dbData) {
        if (null == dbData || dbData.length() < 2 || dbData.charAt(0) != '{' || dbData.charAt(dbData.length() - 1) != '}') {
            throw new IllegalArgumentException("Argument cannot be parsed as array");
        }
        String[] strNumbers = dbData.substring(1, dbData.length() - 1).split(",");
        List<Long> ret = new ArrayList<>(strNumbers.length);
        for (String strNumber : strNumbers) {
            ret.add(Long.parseLong(strNumber));
        }
        return ret;
    }
}
