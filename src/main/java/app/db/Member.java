package app.db;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Getter @Setter @ToString
@EqualsAndHashCode
public class Member implements Serializable {
    private String type;
    private Long ref;
    private String role;

    protected Member() {}

    Member(app.osm.Member member) {
        type = member.getType();
        ref = member.getRef().longValueExact();
        role = member.getRole();
    }

    String toDatabaseString() {
        return "\"(" + this.getType() + "," + this.getRef().toString() + "," + this.getRole() + ")\"";
    }
}
