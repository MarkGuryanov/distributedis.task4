package app.db;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity @Table(name = "test_data")
@Getter @Setter @EqualsAndHashCode
public class TestDataEntity {
    @Id private int id;
    @Basic private String name;
    @Basic private Timestamp cdate;
}
